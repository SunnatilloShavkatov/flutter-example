import 'package:flutter/material.dart';
import 'package:flutter_example/model/size.dart';

class TextScalePage extends StatefulWidget {
  static const String route = "/text/";
  @override
  _TextScalePageState createState() => _TextScalePageState();
}

class _TextScalePageState extends State<TextScalePage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: Text(
          'Scaling text!',
          style: TextStyle(
            fontSize: SizeConfig.safeBlockHorizontal! * 10,
            // color: Colors.white,
          ),
        ),
      ),
    );
  }
}
