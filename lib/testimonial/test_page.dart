import 'package:flutter/material.dart';

class TestPage extends StatefulWidget {
  static const String route = '/test/';

  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF4F5F7),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacer(),
          Text(
            "Our Clients Speak",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 48,
              fontWeight: FontWeight.w800,
              color: Color(0xff18191F),
            ),
          ),
          SizedBox(height: 8),
          Text(
            "We have been working with clients around the world",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color(0xff18191F),
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Spacer(),
              card(),
              SizedBox(width: 30),
              card(),
              SizedBox(width: 30),
              card(),
              Spacer(),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget card() {
    return Container(
      width: 350,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            width: 350,
            height: 192,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(child: Text("Efficient Collaborating")),
                Container(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor neque sed imperdiet nibh lectus feugiat nunc sem.",
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: CircleAvatar(
              child: Icon(
                Icons.person,
                size: 30,
              ),
            ),
          ),
          SizedBox(height: 8),
          Center(
              child: Text(
            "Jane Cooper",
            style: TextStyle(
              color: Color(0xff18191F),
              fontWeight: FontWeight.w700,
              fontSize: 18,
            ),
          )),
          SizedBox(height: 8),
          Center(
            child: Text(
              "CEO at ABC Corporation",
              style: TextStyle(
                color: Color(0xff474A57),
                fontWeight: FontWeight.w400,
                fontSize: 14,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
