import 'package:flutter/material.dart';
import 'package:flutter_example/pass_arguments_page.dart';
import 'package:flutter_example/model/size.dart';
import 'package:flutter_example/testimonial/test_page.dart';
import 'package:flutter_example/text_scale_page.dart';

import 'model/arguments.dart';

class HomePage extends StatefulWidget {
  static const String route = '/home/';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              child: Text("dddddd"),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  PassArgumentsPage.route,
                  arguments: Arguments(
                    'Hello guess! Sunnatillo ',
                  ),
                );
              },
            ),
            ElevatedButton(
              child: Text("Text Scale Page"),
              onPressed: () {
                Navigator.pushNamed(context, TextScalePage.route);
              },
            ),
            SizedBox(height: 20),
            ElevatedButton(
              child: Text("Test Page"),
              onPressed: () => Navigator.pushNamed(context, TestPage.route),
            ),
            SizedBox(height: 20),
            Container(
              height: SizeConfig.safeBlockVertical! * 25,
              width: SizeConfig.safeBlockHorizontal! * 55,
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}
