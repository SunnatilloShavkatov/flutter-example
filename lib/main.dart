import 'package:flutter/material.dart';
import 'package:flutter_example/home_page.dart';
import 'package:flutter_example/pass_arguments_page.dart';
import 'package:flutter_example/testimonial/test_page.dart';
import 'package:flutter_example/text_scale_page.dart';
import 'package:flutter_example/unknown_page.dart';

import 'model/arguments.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      title: "Flutter Example",
      initialRoute: HomePage.route,
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case HomePage.route:
            return MaterialPageRoute(
              builder: (context) => HomePage(),
              settings: settings,
            );
          case PassArgumentsPage.route:
            return MaterialPageRoute(
              builder: (context) => PassArgumentsPage(
                message: (settings.arguments as Arguments).message!,
              ),
              settings: settings,
            );
          case TextScalePage.route:
            return MaterialPageRoute<void>(
              builder: (context) => TextScalePage(),
              settings: settings,
            );
          case TestPage.route:
            return MaterialPageRoute<void>(
              builder: (context) => TestPage(),
              settings: settings,
            );  
          default:
            return MaterialPageRoute(
              builder: (context) => UnknownPage(),
              settings: settings,
            );
        }
      },
    );
  }
}
