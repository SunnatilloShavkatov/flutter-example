import 'package:flutter/material.dart';

class PassArgumentsPage extends StatelessWidget {
  static const String route = '/passArguments/';

  final String? message;

  PassArgumentsPage({
    Key? key,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(message!),
      ),
    );
  }
}
