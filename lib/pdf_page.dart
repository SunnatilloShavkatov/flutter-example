import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class PdfPage extends StatefulWidget {
  static const String route = '/pdf/';

  @override
  _PdfPageState createState() => _PdfPageState();
}

class _PdfPageState extends State<PdfPage> {
  late File file;
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  Future<void> filePicker() async {
    FilePickerResult result = (await FilePicker.platform.pickFiles())!;

    file = File(result.files.single.path!);
  }
}
